# PrepISIMA Puissance4

## Description

This is a Connect4 game (Puissance4) and a Nim game in C language, done as the 1st year's end project of the Prep'ISIMA course

## Features

- Both games support 2 players mode & 1 player versus AI
- Connect4's AI is base on the NegaMax Algorithm, predicting until 11 moves in advance 

## Build

```
gcc main.c -Wall -Wextra -pedantic -Ofast -o main 
```
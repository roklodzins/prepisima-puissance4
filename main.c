// © Romain Klodzinski 2021 - All rights reserved

// Compile without any error/warning on MSBuild (WarningLevel 3)
// Compile without any error/warning on gcc (even with -Wall -Wextra -pedantic)


// import sleep function
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif


#include <stdarg.h> // variadic function
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <time.h>

//see https://en.wikipedia.org/wiki/C_data_types#:~:text=Main%20types%20%20%20%20Type%20%20,%hi%20or%20%hd%20%2010%20more%20rows

typedef unsigned char uint8;
typedef unsigned int  uint16;
typedef unsigned long uint32;

typedef _Bool bool;
typedef char  int8;
typedef int   int16;
typedef long  int32;


// ascii art: https://www.coolgenerator.com/ascii-text-generator
const char* BANNER = "  __  __                    _____      _            _             _ \n |  \\/  |                  |  __ \\    (_)          (_)           | |\n | \\  / | ___ _ __  _   _  | |__) | __ _ _ __   ___ _ _ __   __ _| |\n | |\\/| |/ _ \\ '_ \\| | | | |  ___/ '__| | '_ \\ / __| | '_ \\ / _` | |\n | |  | |  __/ | | | |_| | | |   | |  | | | | | (__| | |_) | (_| | |\n |_|  |_|\\___|_| |_|\\__,_| |_|   |_|  |_|_| |_|\\___|_| .__/ \\__,_|_|\n                                                     | |            \n                                                     |_|\n\nChoisir un jeu :\n\t1: Jeu des batonnets\n\t2: Puissance 4\n\t3: Quitter\n";
const char* NIM_BANNER = "       _                  _             _           _                         _       \n      | |                | |           | |         | |                       | |      \n      | | ___ _   _    __| | ___  ___  | |__   __ _| |_ ___  _ __  _ __   ___| |_ ___ \n  _   | |/ _ \\ | | |  / _` |/ _ \\/ __| | '_ \\ / _` | __/ _ \\| '_ \\| '_ \\ / _ \\ __/ __|\n | |__| |  __/ |_| | | (_| |  __/\\__ \\ | |_) | (_| | || (_) | | | | | | |  __/ |_\\__ \\\n  \\____/ \\___|\\__,_|  \\__,_|\\___||___/ |_.__/ \\__,_|\\__\\___/|_| |_|_| |_|\\___|\\__|___/\n\nChoisir un mode de jeu :\n\t1: Partie : 1 Joueur\n\t2: Partie : 2 Joueurs\n\t3: Retour au menu principal\n";
const char*  P4_BANNER = "  _____       _                                _  _   \n |  __ \\     (_)                              | || |  \n | |__) |   _ _ ___ ___  __ _ _ __   ___ ___  | || |_ \n |  ___/ | | | / __/ __|/ _` | '_ \\ / __/ _ \\ |__   _|\n | |   | |_| | \\__ \\__ \\ (_| | | | | (_|  __/    | |  \n |_|    \\__,_|_|___/___/\\__,_|_| |_|\\___\\___|    |_|\n\nChoisir un mode de jeu :\n\t1: Partie : 1 Joueur\n\t2: Partie : 2 Joueurs\n\t3: Retour au menu principal\n";
const char* LOSE = "  _____    __  __      _ _             _           _                               __ \n |  __ \\  /_/ / _|    (_) |           | |         (_)                             /_ |\n | |  | | ___| |_ __ _ _| |_ ___    __| |_   _     _  ___  _   _  ___ _   _ _ __   | |\n | |  | |/ _ \\  _/ _` | | __/ _ \\  / _` | | | |   | |/ _ \\| | | |/ _ \\ | | | '__|  | |\n | |__| |  __/ || (_| | | ||  __/ | (_| | |_| |   | | (_) | |_| |  __/ |_| | |     | |\n |_____/ \\___|_| \\__,_|_|\\__\\___|  \\__,_|\\__,_|   | |\\___/ \\__,_|\\___|\\__,_|_|     |_|\n                                                 _/ |                                 \n                                                |__/\n\n";
const char* WIN1 = " __      ___      _        _                _           _                               __ \n \\ \\    / (_)    | |      (_)              | |         (_)                             /_ |\n  \\ \\  / / _  ___| |_ ___  _ _ __ ___    __| |_   _     _  ___  _   _  ___ _   _ _ __   | |\n   \\ \\/ / | |/ __| __/ _ \\| | '__/ _ \\  / _` | | | |   | |/ _ \\| | | |/ _ \\ | | | '__|  | |\n    \\  /  | | (__| || (_) | | | |  __/ | (_| | |_| |   | | (_) | |_| |  __/ |_| | |     | |\n     \\/   |_|\\___|\\__\\___/|_|_|  \\___|  \\__,_|\\__,_|   | |\\___/ \\__,_|\\___|\\__,_|_|     |_|\n                                                      _/ |                                 \n                                                     |__/\n\n";
const char* WIN2 = " __      ___      _        _                _           _                               ___\n \\ \\    / (_)    | |      (_)              | |         (_)                             |__ \\\n  \\ \\  / / _  ___| |_ ___  _ _ __ ___    __| |_   _     _  ___  _   _  ___ _   _ _ __     ) |\n   \\ \\/ / | |/ __| __/ _ \\| | '__/ _ \\  / _` | | | |   | |/ _ \\| | | |/ _ \\ | | | '__|   / /\n    \\  /  | | (__| || (_) | | | |  __/ | (_| | |_| |   | | (_) | |_| |  __/ |_| | |     / /_\n     \\/   |_|\\___|\\__\\___/|_|_|  \\___|  \\__,_|\\__,_|   | |\\___/ \\__,_|\\___|\\__,_|_|    |____|\n                                                      _/ |\n                                                     |__/\n\n";
const char* PNUL = "  _____           _   _                    _ _      \n |  __ \\         | | (_)                  | | |     \n | |__) |_ _ _ __| |_ _  ___   _ __  _   _| | | ___ \n |  ___/ _` | '__| __| |/ _ \\ | '_ \\| | | | | |/ _ \\\n | |  | (_| | |  | |_| |  __/ | | | | |_| | | |  __/\n |_|   \\__,_|_|   \\__|_|\\___| |_| |_|\\__,_|_|_|\\___|\n\n";

// Color const
#define WHITE  0
#define BLACK  30
#define RED    31
#define GREEN  32
#define YELLOW 33
#define INDIGO 34
#define PURPLE 35
#define BLUE   36

#define L_WHITE  1
#define L_BLACK  90
#define L_RED    91
#define L_GREEN  92
#define L_YELLOW 93
#define L_INDIGO 94
#define L_PURPLE 95
#define L_BLUE   96

// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| IO UTILS |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#ifdef _WIN32
#define print(...) printf(__VA_ARGS__)    // create an alias : print = printf
#define vprint(...) vprintf(__VA_ARGS__) // create an alias : vprint = vprintf
#else
static inline void vprint(const char* const str, va_list args) { // improve print : flush output
	vprintf(str, args);
	fflush(stdout);
}
static inline void print(const char* const str, ...) {
	va_list args;
	va_start(args, str);
	vprint(str, args);
}
#endif

#ifdef _WIN32
static inline void clear(void) { system("cls"); } // clear screen
#else
static inline void clear(void) { system("clear"); } // clear screen
#endif

static inline void set_color(const uint8 color) { print("\033[%dm", color); } // change text color

void cprint(const uint8 color, const char * const str, ...) {
	va_list arg;
	va_start(arg, str);
	set_color(color);
	vprint(str, arg);
	set_color(WHITE);
}

void pprint(const char* const stt, const bool player, const char* const str, ...) {
	va_list arg;
	va_start(arg, str);
	print("%s", stt);
	cprint(player ? YELLOW : RED, "joueur %d", 1 + player);
	vprint(str, arg);
}

uint8 inputnum(const char* const str, ...) { // return 1 chr, if more or none is given, return 0
	va_list arg;
	va_start(arg, str);
	vprint(str, arg);
	uint8 c = (uint8) getchar();
	if (c == '\n') return 0; // none chr
	uint8 r = (uint8) getchar();
	if (r == '\n') return (uint8) atoi((const char *) &c); // 1 chr
	while ((r = (uint8) getchar()) != '\n') continue; // clear buffer
	return 0;
}


uint8 ask(const uint8 max, const char* const str, ...) { // ask until a valid answer is given (0 < answer < max < 10)
	uint8 answer = 10;
	va_list arg;
	va_start(arg, str);
	clear();
	while (!(answer) || answer > max) {
		if (answer != 10) { clear(); cprint(RED, "Choix invalide\n\n"); } // inputnum return in 0-9
		vprint(str, arg);
		answer = inputnum("\nVotre choix : ");
	}
	return answer;
}

// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| MISC UTILS ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

static inline uint32 randint(const uint32 min, const uint32 max) { return (rand() % (max - min + 1)) + min; } // get an uint32 in [min, max]


static inline uint8 ln2(const uint8 x) {
	for (uint8 i = 0; i < 8; i++) if (x == 1 << i) return i;
	return 0;
}

static inline uint8 countflag(uint32 n) { // count set bit in bitflag
	uint8 count = 0;
	while (n) count++, n &= n - 1;
	return count;
}

static inline void wait(const double seconds) { // crossplatform sleep
#ifdef _WIN32
	Sleep((uint32) (1000 * seconds));
#else
	struct timespec ts = { .tv_sec = (time_t) seconds, .tv_nsec = (int32)((seconds - (int32) seconds) * 1000000000.0) };
	nanosleep(&ts, &ts);
#endif
}


// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| NIM GAME |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

void nim_print(const uint32 bitflag, const uint16 last) { // print nim game UI
	for (uint8 u = 0; u < 7; u++) {
		for (uint8 i = 0; i < 20; i++) cprint((bitflag & (1 << i)) ? L_WHITE : L_BLACK, "%s ", (u == 6) ? "\u2514\u2500\u2518" : (u ? "\u2502 \u2502": "\u250c\u2500\u2510"));
		print("\n");
	}
	set_color(BLUE);
	for (uint8 i = 0; i < 20; i++) print(" %s  ", (i == (last >> 10) || i == (last >> 5 & 0x1f) || i == (last & 0x1f)) ? "\u25b2" : " ");
	set_color(WHITE);
	print("\n");
}

static inline uint8 nim_AI(const uint8 current) { return ((current - 1) % 4) ? (uint8) (current - 1) % 4: (current == 1) ? 1: (uint8) randint(1, 3); }

uint8 nim_ask(const bool player, const bool ai, const uint32 nim, const uint16 last) { // see ask, nim: a 20 bits flag representing remaining bars, last: a 15 bits flag representing last chosen bars
	uint8 answer = 10;
	const uint8 cb = countflag(nim); // get remaining bars number

	clear();
	nim_print(nim, last);

	if (ai && player) { // AI b
		answer = nim_AI(cb);

		print("\nChoix de l'ordinateur");
		for (uint8 i = 0; i < 3; i++) {
			wait(0.25);
			print(".");
		}
		wait(0.25);

		if (cb == 1) print("\n\nL'ordinateur prend le batonnet restant.\n");
		else print("\n\nL'ordinateur a choisi %d batonnet%s.\n", answer, answer > 1 ? "s" : "");

		wait(2);
	}
	else if (cb == 1) {
		answer = 1;
		pprint("\nLe ", player, " prend le batonnet restant.");
		wait(2);
	}
	else while (!answer || answer > ((cb > 2) ? 3 : 2)) { // while answer is invalid
		if (answer != 10) {
			clear();
			cprint(RED, "Choix invalide\n\n");
			nim_print(nim, last);
		}
		print("\nChoisir entre 1 et %d batonnets\n", (cb > 2) ? 3 : 2);
		pprint("\nChoix du ", player, " : ");
		answer = inputnum("");
	}
	return answer;
}

void nim(const bool use_ai) {
	bool current_player = randint(0, 1);
	uint32 current_bar = 0xfffff; // bitflag: (1 << 20) - 1 == 0xfffff
	uint16 last = 0x7fff; // 16e bit mustnt be set;
	uint8 answer, rand;

	while (current_bar) {
		answer = nim_ask(current_player, use_ai, current_bar, last), last = 0x7fff;
		current_player ^= 1;
		for (uint8 i = 0; i < answer; i++) {
			rand = 20;
			while (rand > 19 || !((1 << rand) & current_bar)) rand = (uint8)randint(0, 19); // get a random bar index
			last ^= 0x1f << (5 * i) ^ rand << (5 * i); // update bitflag with new bar index
			current_bar ^= 1 << rand;
		}
	}

	clear();
	if (current_player && use_ai) cprint(BLUE, "%s", LOSE);
	else cprint(GREEN, "%s\n", current_player ? WIN2 : WIN1);

	inputnum("\nAppuyez sur Entrer pour continuer...");
}


// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| PUISSANCE 4 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


typedef struct Score { // Awareness memory
	uint8 column : 3; // 7
	float score;     // -oo
} Score;

typedef struct Cache { uint8 len : 4, ***link; } Cache; // .link[ 0<= combination idx < .len][0-2] -> pointer


// Cached data : provide fast access on grid info
static Cache (* cached_grid)[6] = NULL;  // Cached win combinations per cells					( win_now()  +10% faster )
static uint8 ** cached_line = NULL;     // Cached & inlined grid, order by win combinations	( evaluate() +15% faster )
static uint8 * usage_grid = NULL;      // Store free cells per columns							( Deep_Thought_Awareness + 2% faster )

// Precompiled heuristic values
static const float RLE[] = { 0.0f, 10.0f, 50000.0f, 900000.0f, (float)DBL_MAX };    // Line-eval
static const float RCE[] = { 40.0f, 70.0f, 120.0f, 200.0f, 120.0f, 70.0f, 40.0f }; // Cell-eval

// Global Const
static const uint8 iter_move[] = { 6, 5, 4, 2, 1, 0, 7 }; // Move iter order
static const float INF = (float) DBL_MAX; // Infinite

// Misc Utils
#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static inline bool check(const uint8 a, const uint8 b, const uint8 c, const uint8 d) { return a == b && b == c && c == d; }
static inline uint8 countflag8(uint8 n) { // count set bit (in a 8-bits bitflag)
	uint8 count = 0;
	while (n) count++, n &= n - 1;
	return count;
}


// Strategy & Heuristic Utils
#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static inline bool fast_line_check(uint8 grid[7][6], const uint8 r, uint8* const k, const uint8 i, const uint8 u) { // performs cumulation checks
	if (i >= 7 || u >= 6) return 0;
	if (r == grid[i][u]) {
		if (*k == 3) return 1;
		*k += 1;
	}
	else *k = 0;
	return 0;
}

#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static bool win_if(uint8 grid[7][6], const uint8 c, const bool player) { // 0: Nobody  |  1: Player who play in c win
	uint8 h = 0, v = 0, n = 0, s = 0, k = 0, i = 0, r = player + 1;
	while (k < 6 && grid[c][k]) k++;

	if (k == 6) return 0;  // full column
	grid[c][k] = r;       // Make

	// Perform all checks simultaneously
	while (i < 7 && !(fast_line_check(grid, r, &v, c, i) || fast_line_check(grid, r, &h, i, k) || fast_line_check(grid, r, &n, i + c - k, i) || fast_line_check(grid, r, &s, i + c + k - 5, 5 - i))) i++;

	grid[c][k] = 0;   // Unmake
	return i != 7;   // i == 7 -> no pattern found, no win
}

// Slower (~10%) than win_now version using cached_grid
/*static inline bool win_now(uint8 grid[7][6], const uint8 c, const uint8 k) {	uint8 h = 0, v = 0, n = 0, s = 0, r = grid[c][k];	for (uint8 i = 0; i < 7; i++) if (fast_line_check(grid, r, &v, c, i) || fast_line_check(grid, r, &h, i, k) || fast_line_check(grid, r, &n, i + c - k, i) || fast_line_check(grid, r, &s, i + c + k - 5, 5 - i)) return 1;	return 0;}*/

#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static inline bool win_now(uint8 grid[7][6], const uint8 c, const uint8 k) { // 0: Nobody  |  1: Player who played in (c, k) won
	Cache cx = cached_grid[c][k];
	uint8 r = grid[c][k], l = cx.len;
	for (uint8 i = 0; i < l; i++) if (check(*cx.link[i][0], *cx.link[i][1], *cx.link[i][2], r)) return 1;
	return 0;
}


#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static float evaluate(uint8 grid[7][6]) { // Return (float) heuristique score
	float S = 0;
	uint8 j1, j2;

	for (uint16 c = 0; c < 276; c += 4) {
		j1 = j2 = 0;
		for (uint8 s = 0; s < 4; s++) switch (*cached_line[c + s]) {
			case 1: { j1++; break; }
			case 2: { j2++; break; }
		}
		if (j1 && j2) continue;
		else if (j1) S += RLE[j1];
		else if (j2) S -= RLE[j2];
	}
	for (uint8 u = 0; u < 6; u++) for (uint8 i = 0; i < 7; i++) switch (grid[i][u]) {
		case 1: { S += RCE[i]; break; }
		case 2: { S -= RCE[i]; break; }
	}
	return S;
}


#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static Score Deep_Thought_Awareness(uint8 grid[7][6], const bool player, float alpha, const float beta, const uint8 depth) { // Negamax AI implemented with alpha-beta pruning
	if (!depth) { // Deep Limit
		Score sc = { 0, (player) ? -evaluate(grid) : evaluate(grid) };
		return sc;
	}

	Score sc = { 7, -INF }, rs = { 0, -INF }; // sc : current score  |  rs : recursive score

	for (uint8 i = 3; i != 7 && alpha < beta; i = iter_move[i]) if (usage_grid[i]) { // ordered iter (3, 2, 4, 1, 5, 0, 6), filtered by Alpha-Beta pruning and available columns
		grid[i][rs.column = 6 - usage_grid[i]] = player + 1; // Make ( much faster than "copy & make" )

		usage_grid[i]--;
		rs.score = win_now(grid, i, rs.column) ? INF : -Deep_Thought_Awareness(grid, !player, -beta, -alpha, depth - 1).score; // Deep Thought
		usage_grid[i]++;

		if (rs.score > sc.score) {
			sc.column = i;
			sc.score = rs.score;
			if (sc.score > alpha) alpha = sc.score;
		}

		rs.column = grid[i][rs.column] = 0; // Unmake
	}

	if (sc.column == 7) sc.score = (player) ? -evaluate(grid) : evaluate(grid); // What does it do exactly ?

	return sc;
}

#ifndef _WIN32
#pragma GCC optimize ("Ofast")
#endif
static uint8 p4_AI(uint8 grid[7][6], const bool player) {
	uint8 available_column = 0; // store available columns as 1 in a bitflag
	for (uint8 i = 0; i < 7; i++) available_column |= (!grid[i][5]) << i;

	// Basic Cases

	if (!grid[3][0]) return 4; // start (always start in the middle column)
	if (countflag8(available_column) == 1) for (uint8 i = 0; i < 8; i++) if (available_column == 1 << i) return i + 1;  // end (select the last remaining column)

	// Critical Cases

	for (uint8 i = 0; i < 7; i++) if (available_column >> i & 1 && win_if(grid, i, player)) return i + 1;   // fast win check
	for (uint8 i = 0; i < 7; i++) if (available_column >> i & 1 && win_if(grid, i, !player)) return i + 1; // fast lose check

	// Advanced Cases

	const uint8 inception_depth_level = 11; // min : 1  |  max : 42
	const uint8 clen[7][6] = { {3, 4, 5, 5, 4, 3}, {4, 6, 8, 8, 6, 4}, {5, 8, 11, 11, 8, 5}, {7, 10, 13, 13, 10, 7}, {5, 8, 11, 11, 8, 5}, {4, 6, 8, 8, 6, 4}, {3, 4, 5, 5, 4, 3} };
	
	uint8 ugrid[7] = { 6, 6, 6, 6, 6, 6, 6 }, * cline[276]; // ugrid : store free cells per column
	Cache cgrid[7][6];
	uint16 idx = 0;

	cached_line = cline;
	cached_grid = cgrid;
	usage_grid = ugrid;

	// Init cached_line : allow direct iter on each win combinations
	for (uint8 u = 0; u < 7; u++) for (uint8 i = 0; i < 4; i++) { // +
		if (u < 6) for (uint8 x = 0; x < 4; x++) cline[idx] = &grid[x + i][u], idx++; // horizontal win
		if (i < 3) for (uint8 x = 0; x < 4; x++) cline[idx] = &grid[u][x + i], idx++; // vertical win
	}
	for (uint8 u = 0; u < 4; u++) for (uint8 i = 0; i < 3; i++) { // x
		for (uint8 x = 0; x < 4; x++) cline[idx] = &grid[u + x][x + i], idx++; // diagonal \ win
		for (uint8 x = 0; x < 4; x++) cline[idx] = &grid[u + x][5 - x - i], idx++; // diagonal / win
	}

	// Init usage_grid : allow direct access to available columns and target line
	idx = 42; // recycle : now stores free cells
	for (uint8 i = 0; i < 7; i++) for (uint8 u = 0; u < 6; u++) if (grid[i][u]) idx--, ugrid[i]--;

	// Init cached_grid : allow direct access on each win combinations per cell
	uint16 x; uint8 l;
	for (uint8 i = 0; i < 7; i++) for (uint8 u = 0; u < 6; u++) {
		cgrid[i][u].len = clen[i][u], x = l = 0;
		cgrid[i][u].link = (uint8***) malloc(clen[i][u] * sizeof(uint8**)); // M1

		for (uint8 n = 0; n < clen[i][u]; n++) cgrid[i][u].link[n] = (uint8**)malloc(3 * sizeof(uint8 *)); // M2

		while (l < clen[i][u]) {
			if (cline[x] == &grid[i][u]) {
				x = (x / 4) << 2;
				for (uint8 n = 0; n < 3; n++) {
					if (cline[x] == &grid[i][u]) x++;
					cgrid[i][u].link[l][n] = cline[x], x++;
				} l++, x += (4 - x) % 4;
			} else x++;
		}
	}

	// AI Think
	Score fs = Deep_Thought_Awareness(grid, player, -INF, INF, (inception_depth_level < idx) ? inception_depth_level : idx); // depth = min(MAX_DEPTH, REMAINING_ROUND)
	
	// Free cached data
	cached_line = NULL;  cached_grid = NULL;  usage_grid = NULL;

	for (uint8 i = 0; i < 7; i++) for (uint8 u = 0; u < 6; u++) { // Free cached_grid's malloc
		for (uint8 x = 0; x < cgrid[i][u].len; x++) free(cgrid[i][u].link[x]); // F2
		free(cgrid[i][u].link); // F1
	}

	// Return Deep Though's answer
	if (fs.column > 6) { // Maybe never ?
		uint8 r;
		do { r = (rand() % 7) + 1; } while (!(available_column >> (r - 1) & 1)); // default
		return r;
	}
	else return 1 + fs.column;
}


static inline bool is_full_array(const uint8 grid[6]) { return (bool) grid[5]; }

void print_grid(uint8 grid[7][6], const uint8 last) {
	for (uint8 i = 1; i < 8; i++) cprint((last == i) ? RED : ((is_full_array(grid[i-1])) ? L_BLACK : L_WHITE), "  %s%d", (i > 1) ? " ": "", i); // print number line

	set_color(BLUE);
	print("\n");

	for (uint8 u = 0; u < 6; u++) {
		for (uint8 i = 0; i < 7; i++) print("%s\u2500\u2500\u2500", (i > 0) ? "\u253c": "\u251c"); // Top & Inter Lines
		print("\u2524\n");

		for (uint8 i = 0; i < 7; i++) { // print Cells
			print("\u2502 ");
			set_color((grid[i][5 - u] == 2) ? YELLOW : RED);

			print("%s ", (grid[i][5 - u]) ? "\u25cf": " ");
			set_color(BLUE);
		}
		print("\u2502\n");
	}

	for (uint8 i = 0; i < 7; i++) print("%s\u2500\u2500\u2500", i ? "\u2534":  "\u2514"); // Bottom Line
	print("\u2518\n");

	set_color(WHITE);
}

static inline bool line_check(uint8 grid[7][6], uint8* k, const uint8 i, const uint8 u) { // performs cumulation checks
	if (i >= 7 || u >= 6) return 0;
	if ((*k & 3) == grid[i][u]) {
		*k += 4;
		if (*k >> 4 && *k & 3) return 1;
	}
	else *k = ((bool)grid[i][u]) << 2 | grid[i][u];
	return 0;
}

uint8 get_winner(uint8 grid[7][6]) { // 0: Nobody  |  1: Player 1  |  2: Player 2
	uint8 h, v, n, s; // ⟷, ⬍, ⬈, ⬉    |    bitflags: 000LLLRR (L: lines Len (0 to 4), R: player ID (Return))

	for (uint8 i = 0; i < 7; i++) {     // iter a single 7x7 to avoid the double usage of 7x6 and 6x7 (yes it works)
		h = 0, v = 0, n = 0, s = 0;
		for (uint8 u = 0; u < 7; u++) {
			if (line_check(grid, &v, i, u)) return v & 3;                 // ⬍ vertical
			if (i == 6) continue;										 // (i < 6: avoid the last & always false diagonal check)
			if (line_check(grid, &h, u, i)) return h & 3;               //  ⟷ horizontal
			if (line_check(grid, &n, i + u - 2, u)) return n & 3;      //    ⬈ diagonal
			if (line_check(grid, &s, i + u - 2, 5 - u)) return s & 3; //     ⬉ diagonal
		}
	}
	return 0;
}

uint8 p4_ask(const bool player, const bool ai, uint8 grid[7][6], const uint8 last) { // see ask, ai : if 0, ai is replace by player 2 | last is the column chose at previous turn
	uint8 answer = 10, available_column = 0; // store available columns as 1 in a bitflag
	for (uint8 i = 0; i < 7; i++) available_column |= (!is_full_array(grid[i])) << i;

	clear();
	print_grid(grid, last);

	if (ai && player) { // AI
		print("\nChoix de l'ordinateur");
		
		for (uint8 i = 0; i < 3; i++) {
			wait(0.25);
			print(".");
		}
		answer = p4_AI(grid, 1);

		if (countflag(available_column) < 2) print("\n\nL'ordinateur choisit la colonne restante : %d\n", answer);
		else print("\n\nL'ordinateur a choisi la %d%s colonne\n", answer, answer > 1 ? "\u1d49": "\u02b3\u1d49");

		wait(2);
	}
	else if (countflag(available_column) < 2) {
		answer = ln2(available_column) + 1;
		pprint("\nLe ", player, " choisit la colonne restante : %d", answer);
		wait(2);
	}
	else while (!(answer && available_column >> (answer - 1) & 1)) { // while answer is invalid
		if (answer != 10) {
			clear();
			cprint(RED, "Choix invalide\n\n");
			print_grid(grid, last);
		}
		pprint("\nChoisir une colonne, de 1 à 7\n\nChoix du ", player, " : ");
		answer = inputnum("");
	}
	return answer;
}


void p4(const bool use_ai) {
	uint8 grid[7][6] = {{0}};
	uint8 answer = 0;
	bool current_player = randint(0, 1);

	for (uint8 r = 0; r < 42 && !get_winner(grid); r++) { // because 42 is the ultimate answer of life, universe and everything (or it's just 6x7)
		answer = p4_ask(current_player, use_ai, grid, answer);
		for (uint8 i = 0; i < 6; i++) if (!grid[answer - 1][i]) { grid[answer - 1][i] = 1 + current_player; break; }
		current_player ^= 1;
	}

	clear();
	switch (get_winner(grid)) {
		case 0: cprint(PURPLE, "%s\n", PNUL); break;
		case 1: cprint(GREEN,  "%s\n", WIN1); break;
		case 2: cprint((use_ai) ? BLUE: GREEN, "%s\n", (use_ai) ? LOSE: WIN2); break;
	}
	print_grid(grid, answer);
	inputnum("\nAppuyez sur Entrer pour continuer...");
}


// main
int launcher(const char* banner, void callback(const bool)) {
	while (1) switch (ask(3, banner)) {
		case 1: callback(1); break;
		case 2: callback(0); break;
		case 3: return 0;
	}
}


int main(void) {
	#ifdef _WIN32
		SetConsoleOutputCP(65001); // Important : correct unicode on windows
	#endif

	srand((uint32) time(0));
	
	while (1) switch (ask(3, BANNER)) {
		case 1: launcher(NIM_BANNER, nim); break;
		case 2: launcher(P4_BANNER, p4); break;
		case 3: return 0;
	}
}
